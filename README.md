# TIP Deconvolution Software with GUI


(c) Dean Wilding, Oleg Soloviev 2016-18

------------------------------------------------------------------------------------


This piece of sofware is able to process, single, multiple and colour TIFF files
to produce a deconvolved version using the methodology described in the following 
article:


Dean Wilding, Oleg Soloviev, Paolo Pozzi, Gleb Vdovin, and Michel Verhaegen, 
"Blind multi-frame deconvolution by tangential iterative projections (TIP)," 
*Opt. Express 25,* 32305-32322 **(2017)**

